#
# Automatically generated file. DO NOT MODIFY
#

LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),lisa)

$(call add-radio-file-sha1-checked,radio/abl.img,58cfc12c2d7622eeed4aeab0a23439e7ea5cbedd)
$(call add-radio-file-sha1-checked,radio/aop.img,d25ffdcab31a03fc230b5baa538bfb35736f61cc)
$(call add-radio-file-sha1-checked,radio/bluetooth.img,888531dd71ba8481a4a66a78d4a3a4a0d112351b)
$(call add-radio-file-sha1-checked,radio/cpucp.img,f0a017bf2c47f4431c4ea5184dd873475f55017f)
$(call add-radio-file-sha1-checked,radio/devcfg.img,63956c527ee227c4d83c6f074efb0275fd74ffac)
$(call add-radio-file-sha1-checked,radio/dsp.img,c21e6e8bf124b76385a8f00c14baf084e9623352)
$(call add-radio-file-sha1-checked,radio/featenabler.img,9ce5b20f5c2f75ca7125bf34ab017cfff988b022)
$(call add-radio-file-sha1-checked,radio/hyp.img,7e3e76addba13c6901b448432e93f2a012e05500)
$(call add-radio-file-sha1-checked,radio/imagefv.img,fed435caf5796892332c5f4820e881d9b68c6c14)
$(call add-radio-file-sha1-checked,radio/keymaster.img,42a5686e31c1cb30bd2ddf597e6a9fe499184c77)
$(call add-radio-file-sha1-checked,radio/modem.img,781a0e405c6ea35c4a47d069b499a45fde60c4ba)
$(call add-radio-file-sha1-checked,radio/qupfw.img,b7f897a6434d611f436747222ca69355dddd8a84)
$(call add-radio-file-sha1-checked,radio/shrm.img,7c1197e6758f787ea7ca8aaa692ca671e7cbd00d)
$(call add-radio-file-sha1-checked,radio/tz.img,c68ab15bfabaf4a74bf9fabdee6ff4e668d08271)
$(call add-radio-file-sha1-checked,radio/uefisecapp.img,97e9b8947abac17a3800b73e4ccc4effd3e8b927)
$(call add-radio-file-sha1-checked,radio/xbl.img,e72f67d2ef016758c1a69bfda35d3e2ee8480279)
$(call add-radio-file-sha1-checked,radio/xbl_config.img,30b82f55c493c559a474c2bbcbf78c4f03ac512d)

endif
